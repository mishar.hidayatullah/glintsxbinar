"use strict";
const faker = require("faker");

module.exports = {
  up: async (queryInterface, Sequelize) => {
    // Add 3 data to suppliers
    await queryInterface.bulkInsert("siswa", [
      {
        name: faker.name.findName(), // generate random name
        tanggalLahir: faker.date.recent(),
        alamat: faker.address.streetAddress(),
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: faker.name.findName(), // generate random name
        tanggalLahir: faker.date.recent(),
        alamat: faker.address.streetAddress(),
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: faker.name.findName(), // generate random name
        tanggalLahir: faker.date.recent(),
        alamat: faker.address.streetAddress(),
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: faker.name.findName(), // generate random name
        tanggalLahir: faker.date.recent(),
        alamat: faker.address.streetAddress(),
        createdAt: new Date(),
        updatedAt: new Date(),
      },
    ]);
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete("siswa", null, {});
  },
};
