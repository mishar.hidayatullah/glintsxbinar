// function cube

function cube(s, s, s) {
    console.log(s * s * s);
  }
  
  cube(3, 3, 3);
  cube(13, 13, 13);
  
  // function return
  
  function cubeReturn(s, s, s) {
    console.log("return: " + s * s * s);
    return s * s * s;
  }
  
  let cubeOne = cubeReturn(10, 11, 12);
  let cubeTwo = cubeReturn(11, 12, 13);
  console.log("Cube 1 + Cube 2 :" + (cubeOne + cubeTwo));
  
  console.log(
    "------------------------------------------------------------------------------------"
  );
  
  // function kerucut
  const phi = 3.14;
  function kerucut(r) {
    console.log((1 / 3) * phi * r * r);
  }
  kerucut(6);
  
  // function return 
  function kerucutReturn(r) {
    console.log("return: " + (1 / 3) * r * r);
    return (4 / 3) * r * r;
  }
  
  let kerucutOne = kerucutReturn(3);
  let kerucutTwo = kerucutReturn(13);
  console.log("Kerucut 1 + Kerucut 2 :" + (kerucutOne + kerucutTwo));
  