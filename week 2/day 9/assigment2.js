const events = require ("../day 10/events")

let rapidTest = [
    {
        "name": "John",
        "status": "Positive"
    },
    {
        "name": "Mike",
        "status": "Suspect"
    },
    {
        "name": "daniel",
        "status": "Negative"
    },
    {
        "name": "jonathan",
        "status": "Suspect"
    },
    {
        "name": "Michael",
        "status": "Positive"
    },
    {
        "name": "Ciel",
        "status": "Positive"
    },
    {
        "name": "Kuyt",
        "status": "Negative"
    },
    {
        "name": "Ferdinan",
        "status": "Suspect"
    },
    {
        "name": "ferant",
        "status": "Negative"
    }
  
   ]
  
function pasien (hasilTest) {
    let a = [];
    for (let i = 0; i<rapidTest.length; i++){
        if (rapidTest[i].status == hasilTest ){
            a.push(rapidTest[i].name)
            //console.log(`berikut ${hasilTest} : ${rapidTest[i].name}`);
        }
    }
    return a
}

// pasien("Positive");
// pasien("Negative");
// pasien("Suspect");   

module.exports = {pasien};
   
   
   
   