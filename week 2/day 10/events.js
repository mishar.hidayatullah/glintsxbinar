const EventEmitter = require('events');
const readline = require('readline');
const pasien = require('../day 9/assigment2');
const a = require('../day 9/assigment2')

const my = new EventEmitter();
// make rl
const rl = readline.createInterface({
    input : process.stdin,
    output : process.stdout,
});

//listener
my.on('login:failed', function (email) {
    console.log(`${email} is failed to login`);
    rl.close();
});

my.on('login:success', function (email) {
    console.log(`${email} is success to login`);
    console.log(`pasien Negative`);
    console.log(a.pasien("Negative"));
    console.log(`pasien Positive`);
    console.log(a.pasien("Positive"));
    console.log(`pasien Suspect`);
    console.log(a.pasien("Suspect"));
    rl.close();
})

// function login
function login(email, password) {
    const passwordInDatabase = '336699';

    if (password !== passwordInDatabase) {
        my.emit('login:failed', email);
    } else {
        my.emit('login:success', email);
    }
}


rl.question('Email: ', (email) => {
    rl.question('Password: ', (password) => {
        login(email, password);
    });
});