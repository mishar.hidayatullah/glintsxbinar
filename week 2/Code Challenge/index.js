const data = require('./lib/arrayFactory.js');
const test = require('./lib/test.js');

/*
 * Code Here!
 * */



function clean(data) {
  // Code here
  let newData = [];
  for (let i = 0; i < data.length; i++){
      if (data[i] != null){
          newData.push(data[i]);
      }
  }
//   data cleansing
return newData;
}
console.log(clean(data));



// Optional
function clean(data) {
  return data.filter(i => typeof i === 'number');
}

// Should return array
function sortAscending(data) {
  // Code Here
  function bubbleSort(data) {
    const jumlahData = data.length;

    let terdeteksiPerubahan = false;

    for(let i = 0 ; i < jumlahData; i++) {

        for(let j = 0; j < jumlahData - i - 1; j++) {

            // Ascending >
            // Descending <
            if( data[j] > data[j+1] )
            {
                // swap the elements
                const temp = data[j];
                data[j] = data[j+1];
                data[j+1] = temp;
                // if swapping happens update flag to 1
                terdeteksiPerubahan = true;
            }
        }

        if(!terdeteksiPerubahan){
            break;
        }

    }

    return data;
}

const filteredData = data.filter((item) => item != null)

console.log(bubbleSort(filteredData))
};

// Should return array
function sortDecending(data) {
  // Code Here
  function bubbleSort(data) {
    const jumlahData = data.length;

    let terdeteksiPerubahan = false;

    for(let i = 0 ; i < jumlahData; i++) {

        for(let j = 0; j < jumlahData - i - 1; j++) {

            // Ascending >
            // Descending <
            if( data[j] < data[j+1] )
            {
                // swap the elements
                const temp = data[j];
                data[j] = data[j+1];
                data[j+1] = temp;
                // if swapping happens update flag to 1
                terdeteksiPerubahan = true;
            }
        }

        if(!terdeteksiPerubahan){
            break;
        }

    }

    return data;
}

const filteredData = data.filter((item) => item != null)

console.log(bubbleSort(filteredData))
}

// DON'T CHANGE
test(sortAscending, sortDecending, data);
