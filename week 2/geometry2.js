/* If we use callback function, we can't add another logic outside the function */

// Import readline
const readline = require('readline');
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

// Function to calculate volume kerucut
const phi = 3.14;
function kerucut(phi, radius, tinggi) {
  return phi * radius * tinggi * (1/3);
}

/* Alternative Way */
// All input just in one code
function input() {
  rl.question('Radius: ', function (radius) {
      rl.question('Tinggi: ', (tinggi) => {
        if (radius > 0  && tinggi > 0) {
          console.log(`\nKerucut: ${kerucut(radius, phi, tinggi)}`);
          rl.close();
        } else {
          console.log(`radius dan tinggi harus berisi angka\n`);
          input();
        }
      });
    });
  };
/* End Alternative Way */

console.log(`Rectangle`);
console.log(`=========`);
input(); // Call Alternative Way
