/* If we use callback function, we can't add another logic outside the function */

// Import readline
const readline = require('readline');
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

// Function to calculate beam volume
function cube(sisi, sisi, sisi) {
  return sisi ** 3;
}

/* Way 1 */
// Function for inputing length of beam
function inputSisi() {
    rl.question(`Sisi: `, (sisi) => {
      if (!isNaN(sisi)) {
          console.log(`\nCube: ${cube(sisi, sisi, sisi)}`);
          rl.close();
        } else {
          console.log(`Sisi harus berisi angka\n`);
          inputSisi(sisi);
        }
      });
    }
  



console.log(`Cube`);
console.log(`=========`);
inputSisi(); // Call way 1

